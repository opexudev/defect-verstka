$(document).ready(function() {
    $('a[href="#"], input[type="submit"]').click(pd); // УДАЛИТЬ НА ПРОДАКШН ВЕРСИИ
    $('.front-slider').slick({
        useTransform: false
    });
    $('.history-slider').slick({
        useTransform: false,
        dots: true,
        arrows: false
    });

    $(".set_height").each(function(){ // ровняем высоту колонок по более высокой
        $(this).height($(this).next(".base_height").height());
    })

    getsquared();                   // превращаем блок в квадрат
    $(window).resize(function(){    // также при изменении размеров экрана
        getsquared();
    });

    function pd(e){                 // disable defaults
        e.preventDefault();
    };

    function getsquared(){
        if($(window).width()<500){
            var rbwidth = $('.bg-crimson').width();
            $(".bg-crimson").height(rbwidth);
        }
    }
})